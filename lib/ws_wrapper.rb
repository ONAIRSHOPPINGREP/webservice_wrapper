require 'json'
require 'action_controller'

# 
# 
# 
module WsWrapper
  @@logger= nil

  def self.logger= logger=nil
    @@logger= logger
  end
  def self.logger
    @@logger || Rails.logger
  end

  #########################################################################
  # Builders
  #########################################################################
  # Build pagination Hash object
  # [total] Required Total of element
  # [page] Required Current page
  # [page_items] Required Total of elements in this page
  # * return => Return the pagination Hash
  def build_pagination total, page, page_items
    {
      page: page,
      page_items: page_items,
      total_items: total
    }
  end

  # Build error Hash object
  # [exp] Required Exception object
  # [code] Optional Code for add more info to exception
  # [message] Optional Overwrite the Exception message
  # * return => Return the error Hash
  def build_error exception, message=nil, code=nil
    backtrace= "#{exception.message} >> #{exception.backtrace}"
    WsWrapper.logger.error "ERROR:#{message.nil? ? '': message} #{backtrace}"
    hash = { type: exception.class.name, backtrace: backtrace, message: exception.message}

    hash.merge!({ code: code }) unless code.nil?
    hash.merge!({ message: message }) unless message.nil?

    hash
  end

  #########################################################################
  # Response methods
  #########################################################################
  # Build success JSON response
  # [data] Required Data that will be returned
  # [pagination] Optional Pagination object use *build_pagination* method
  # * return => Success response as JSON
  def success_response data, pagination=nil
    hash = { success: true, data: data }
    hash.merge!({ pagination: pagination }) unless pagination.nil?
    hash.to_json
  end

  # Build failure JSON response
  # [error] Optional Error object, use *build_error* method
  # * return => Failure response as JSON
  def failure_response error=nil
    hash = { success: false }
    hash.merge!({ error: error }) unless error.nil?
    hash.to_json
  end

  #########################################################################
  # Utilities
  #########################################################################
  # Method used if some raise occurs inside controller
  def error_render_method(exception)
    render :json => failure_response(build_error(exception))
  end

  # Method used in case that route not exists
  def raise_not_found!
    raise ActionController::RoutingError.new("Invalid route #{request.path}")
  end

end
