# WsWrapper

This gem will be used as wrapper (for now only JSON) in WebServices application.
We prefer to work with the OpenAPI Specification: https://github.com/OAI/OpenAPI-Specification/tree/master/versions.

## Installation

Add this line to your application's Gemfile:

    gem 'ws_wrapper', :git => 'git@gitlab.com:ONAIRSHOPPINGREP/webservice_wrapper.git'

And then execute:

    $ bundle

Or install it yourself as:

    $ git clone git@gitlab.com:ONAIRSHOPPINGREP/webservice_wrapper.git
    $ cd webservice_wrapper
    $ gem build ws_wrapper.gemspec
    $ gem install ws_wrapper-x.x.x.gem

## Contributing
*ssh://**username**git@gitlab.com:ONAIRSHOPPINGREP/webservice_wrapper.git*

## Usage
This is an example of Rails application, step by step.

### Routes
Add two lines at the end of the :ws namespace to catch all invalid routes, by this way we always return JSON.

Inside your **routes** file see the two ```match``` at the end of the ```namespace``` block:

```
  namespace :ws do
    namespace :v1 do
      
      resources :products, :only => [] do
        get 'test_success', on: :collection
        get 'test_failure', on: :collection
        get 'test_raise', on: :collection
      end

      match '/', :to => 'base#raise_not_found!', via: :all
      match '*other', :to => 'base#raise_not_found!', via: :all
    end
  end
```

### BaseController (Generic)
You can use a generic controller that all other webservice controllers should extend.
Which will be used for generic things: check if user is logged-in, define webservice response format, parsing requests, etc.

To avoid returning HTML when exceptions are not catched, the BaseController should declare ```rescue all exceptions```:
```
require 'ws_wrapper'

module Ws
  module V1
    class BaseController < ApplicationController
      include WsWrapper
      rescue_from Exception, :with => :ws_error_render
    
      def ws_error_render(exception=nil, code=nil)
        # return some json with error information
      end
    end
  end
end
```


### Controllers (Specific)
Now in the specific controllers use the methods available in WsWrapper.

This is an example for an imaginary **products controller**:

```
module Ws
  module V1
    class ProductsController < Ws::V1::BaseController 

        def test_success
          render :json => success_response('test')
        end
      
        def test_failure
          render :json => failure_response
        end
      
        def test_raise_1
          raise "Raise Test Error"
        end
        def test_raise_2
          error = build_error Exception.new("Exeption test")
          render :json => failure_response(error)
        end
    end
  end
end
```

### Advices
 - Force that all response inside 'ws/v1' return JSON using *:default => {:format => :json}* or other options
 
### Methods

```
  #########################################################################
  # Builders
  #########################################################################
  # Build pagination Hash object
  # [total] Required Total of element
  # [page] Required Current page
  # [page_items] Required Total of elements in this page
  # * return => Return the pagination Hash
  build_pagination total, page, page_items

  # Build error Hash object
  # [exp] Required Exception object
  # [code] Optional Code for add more info to exception
  # [message] Optional Overwrite the Exception message
  # * return => Return the error Hash
  build_error exp, code=nil, message=nil

  #########################################################################
  # Response methods
  #########################################################################
  # Build success JSON response
  # [data] Required Data that will be returned
  # [pagination] Optional Pagination object use *build_pagination* method
  # * return => Success response as JSON
  success_response data, pagination=nil

  # Build failure JSON response
  # [error] Optional Error object, use *build_error* method
  # * return => Failure response as JSON
  failure_response error=nil

  #########################################################################
  # Utilities
  #########################################################################
  # Method used if some raise occurs inside controller
  error_render_method(exception)

  # Method used in case that route not exists
  raise_not_found!
```